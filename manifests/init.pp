class gtest ( $source_dir = $gtest::params::source_dir,
              $build_dir  = $gtest::params::build_dir,
              $target_dir = $gtest::params::target_dir,
              $manage_packages = $gtest::params::manage_packages) inherits ::gtest::params
{
  if($manage_packages){
    $gtest::params::required_packages.each | String $p | {
      if(!Package["$p"]){
        package { "$p" :
          ensure => 'installed',
        }
      }
    }
  }

  if($gtest::params::build_shared_library){
    file { '/tmp/build_gtest.sh':
      ensure => file,
      content => template('gtest/build_gtest.sh.erb'),
      mode => '777'
    }

    exec { 'gtest_build_and_install' :
      command     => '/tmp/build_gtest.sh',
      path        => '/usr/bin:/usr/sbin:/bin:/usr/local/bin',
      unless      => "ls ${taget_dir}/libg* 2>/dev/null",
      require     => [File['/tmp/build_gtest.sh'],Package['gcc'],Package['cmake']]
    }
  }
}