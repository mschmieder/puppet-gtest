class gtest::params {
  case $::osfamily {
    'Debian': {
        $source_dir='/usr/src/gtest'
        $target_dir='/usr/lib'
        $build_dir='/tmp/gtest-build'
        $required_packages=['libgtest-dev','gcc','cmake']
        $build_shared_library=true
        $manage_packages=true
   }
     default: { fail("unsupported platform ${::osfamily}") }
  }
}